/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_init.h"
#include <peripheral_clk_config.h>
#include <utils.h>
#include <hal_init.h>

struct ac_sync_descriptor AC_0;

void AC_0_PORT_init(void)
{

	// Disable digital pin circuitry
	gpio_set_pin_direction(PA04, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(PA04, PINMUX_PA04B_AC_AIN0);

	// Disable digital pin circuitry
	gpio_set_pin_direction(PA05, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(PA05, PINMUX_PA05B_AC_AIN1);

	// Disable digital pin circuitry
	gpio_set_pin_direction(PA06, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(PA06, PINMUX_PA06B_AC_AIN2);

	// Disable digital pin circuitry
	gpio_set_pin_direction(PA07, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(PA07, PINMUX_PA07B_AC_AIN3);

	gpio_set_pin_level(PA18,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(PA18, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(PA18, PINMUX_PA18H_AC_CMP0);

	gpio_set_pin_level(PA19,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(PA19, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(PA19, PINMUX_PA19H_AC_CMP1);
}

void AC_0_CLOCK_init(void)
{

	hri_mclk_set_APBAMASK_AC_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, AC_GCLK_ID, CONF_GCLK_AC_SRC | (1 << GCLK_PCHCTRL_CHEN_Pos));
}

void AC_0_init(void)
{
	AC_0_CLOCK_init();
	ac_sync_init(&AC_0, AC);
	AC_0_PORT_init();
}

void system_init(void)
{
	init_mcu();

	AC_0_init();
}
