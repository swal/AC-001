#include <atmel_start.h>

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();

	/* enable the hardware */
	ac_sync_enable(&AC_0);
	ac_sync_start_comparison(&AC_0, 0);

	while (true) {
//		ac_sync_get_result(&AC_0, 0); /* this is not necessary to use the hardware only */
	}
}
