/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_examples.h"
#include "driver_init.h"
#include "utils.h"

/**
 * Example of using AC_0 to generate waveform.
 */
void AC_0_example(void)
{
	ac_sync_enable(&AC_0);
	ac_sync_start_comparison(&AC_0, 0);

	while (true) {
		ac_sync_get_result(&AC_0, 0);
	}
}
