Evaluation of the analog comparator.


<table>
<tr><th>AC Signal</th><th>MCU Pin</th><th>SAM L10 Xplained Pin</th><th>Signal</th></tr>
<tr><td>AIN/0    </td><td>PA04   </td><td>EXT1, Pin 17        </td><td>Comp 0, positive input</td></tr>
<tr><td>CMP/0    </td><td>PA18   </td><td>EXT1, Pin 7         </td><td>Comp 0, output</td></tr>
</table>

